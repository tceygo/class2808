import React from 'react';
import "./city.css"

const City = ({cityInfo}) =>{
    return(
    <ul>
        {cityInfo.map((element, index)=> {
            //console.log(element)
             return(<li className="itemCity" key = {index*5+"g"}>{element.id} {element.name}</li>)
            })}
    </ul>
    )
}

    


export default City