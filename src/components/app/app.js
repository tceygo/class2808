import React from 'react'; 

import Region from '../region/region';

import data from '../../data';


//console.dir(data)

const App = () => {
    return(
        
        <div>
            <Region regionInfo = {data.areas} ></Region>
        </div>
    )
}

export default App
