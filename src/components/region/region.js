import React from 'react';
import City from "../city/city"

import "./region.css"

const Region = ({regionInfo}) => {
   
    
    //console.dir(regionInfo)
    return (
    <ol>
        {regionInfo.map((element, index)=> {
            //console.log(element)
             return(<li className="itemRegion" key = {index*5+"h"}>{element.id} {element.name}
             <City cityInfo={element.areas}/>
             </li>)
            })}
    </ol>
    )
}

export default Region
